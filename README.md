# :sunny: JD: PORTFOLIO + DEMO  :house:
      

---
#### Front End / UI Development, Full Stack Engineering, Creative Design
    
---      
- ## Content Management Systems
    * Headless CMS (WordPress Backend)      
      
---
- ## Widgets
    * Weather App
    * Chronometer
    * Metronome     
    
---
- ## Web Apps
    * Eventbrite API
    * Current Events Website - News API
    * Shopping Cart/E-commerce    
    
---
- ## Games / Entertainment
    * Duck Sauce (DS9-inspired Dabo game of chance)    
     
---
