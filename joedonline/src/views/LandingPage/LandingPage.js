import React from 'react'

// JoeDLayouts
import {
  Container,
  Layout,
  FlexWrapper,
  FlexWrapper as MainWrapper,
  Wrapper,
} from '../../components/JoeDLayouts/JoeDLayouts'

// JoeDMaterials
import {
  JoeDIcon as HomeIcon,
  JoeDIcon as GitHubIcon,
  JoeDIcon as GmailIcon,
  JoeDIcon as LinkedInIcon,
  JoeDIcon as PersonalInfoIcon,
  JoeDIcon,
} from '../../components/JoeDMaterials/JoeDIcon/JoeDIcon'

import {
  JoeDCard as Card,
} from '../../components/JoeDMaterials/JoeDCards/JoeDCards'


// LandingPage
export default function LandingPage(props) {
  const iconArgs = {
    size: "3.5em", // 56px
    isPointer: true,
    fill: "limegreen",
  }

  const homeArgs = {
    ...iconArgs,
    name: "Home",
  }

  const githubArgs = {
    ...iconArgs,
    name: "GitHub",
  }

  const gmailArgs = {
    ...iconArgs,
    name: "Gmail",
  }

  const linkInArgs = {
    ...iconArgs,
    name: "LinkedIn",
  }

  const personalInfoArgs = {
    ...iconArgs,
    name: "PersonalInfo",
  }

  const TestComponent = <Layout direction="vertical">
    <Container fullHeight="elastic" centered="main-cross" padding="normal">
      <MainWrapper partition={ 5 }>
        <Card />
        <Card />
        <Card />
        <Card />
        <Card />
      </MainWrapper>
    </Container>
    <Container gutterLeft="normal" centered="main"><HomeIcon { ...homeArgs } /></Container>
    <Container gutterRight="narrow" gutterLeft="narrow" align="main-end"><GmailIcon { ...gmailArgs } /></Container>
    <Container gutterBottom="wide"><LinkedInIcon { ...linkInArgs } /></Container>
    <Container align="main-end"><GitHubIcon { ...githubArgs } /></Container>
    <Container centered="main-cross"><PersonalInfoIcon { ...personalInfoArgs } /></Container>
    <Container><JoeDIcon /></Container>
    <Container fullHeight="elastic">
      <MainWrapper partition={ 5 }>
        <FlexWrapper partition={ 1 }>FLEX WRAPPER: partition = 1</FlexWrapper>
        <FlexWrapper partition={ 2 }>FLEX WRAPPER: partition = 2</FlexWrapper>
        <FlexWrapper partition={ 3 }>FLEX WRAPPER: partition = 3</FlexWrapper>
        <FlexWrapper partition={ 4 }>FLEX WRAPPER: partition = 4</FlexWrapper>
        <FlexWrapper partition={ 5 }>FLEX WRAPPER: partition = 5</FlexWrapper>
      </MainWrapper>
    </Container>
    <Container fullHeight="elastic">
      <MainWrapper partition={ 5 }>
        <Wrapper>
          <FlexWrapper partition={ 1 }>
            (FlexWrapper: partition = 1) inside a WRAPPER component.
          </FlexWrapper>
          <FlexWrapper partition={ 4 }>
            (FlexWrapper: partition = 4) inside a WRAPPER component.
          </FlexWrapper>
        </Wrapper>
        <Wrapper>
          <FlexWrapper partition={ 3 }>
            (FlexWrapper: partition = 3) inside a WRAPPER component.
          </FlexWrapper>
          <FlexWrapper partition={ 2 }>
            (FlexWrapper: partition = 2) inside a WRAPPER component.
          </FlexWrapper>
        </Wrapper>
        <Wrapper>
          <FlexWrapper partition={ 5 }>
            (FlexWrapper: partition = 5) inside a WRAPPER component.
          </FlexWrapper>
        </Wrapper>
      </MainWrapper>
    </Container>
  </Layout>

  return <>{ TestComponent }</>
} // END LandingPage
