import React from 'react'
import './JoeDIcon.scss'

// ICONS
import {
  Home as HomeIcon,
  Generic as GenericIcon,
  GitHub as GitHubIcon,
  Gmail as GmailIcon,
  LinkedIn as LinkedInIcon,
  PersonalInfo as PersonalInfoIcon,
} from './icons/icons'


function IconFactory(props) {
  switch(props.name) {
    case "Home": return <HomeIcon fill={ props.fill } />
    case "GitHub": return <GitHubIcon fill={ props.fill } />
    case "Gmail": return <GmailIcon fill={ props.fill } />
    case "LinkedIn": return <LinkedInIcon fill={ props.fill } />
    case "PersonalInfo": return <PersonalInfoIcon fill={ props.fill } />
    default: return <GenericIcon fill={ props.fill } />
  }
}


// EXPORT...
const iconstyle = props => ({
  width: props.size ? props.size : "3.5em", // 56px
  height: props.size ? props.size : "3.5em", // 56px
  cursor: props.isPointer ? 'pointer' : "",
})

export const JoeDIcon = props => <>
  <hp-icon style={ iconstyle(props) }>{ IconFactory(props) }</hp-icon>
</>
