import React from 'react'
import './JoeDCards.scss'

// JoeDLayouts
import { FlexWrapper, Layout, Wrapper } from '../../JoeDLayouts/JoeDLayouts'

export function JoeDCard(props) {
  return <FlexWrapper partition={ 1 }>
    <Wrapper>
      <div className="JoeDCards">
        <div className="inner-wrap">
        <Layout direction="vertical">
          <header></header>
          <hp-body></hp-body>
        </Layout>
        </div>
      </div>
    </Wrapper>
  </FlexWrapper>
}
