import React from 'react'
import { ContainerClasses, LayoutClasses, FlexWrapperClasses } from './_functions'
import './scss/index.scss'


export const Container = props => (
  <div className={ `Container ${ContainerClasses(props).join(" ").trim()}`.trim() }>
    { props.children }
  </div>
) // END Container

export const FlexWrapper = props => (
  <div className={ `FlexWrapper ${FlexWrapperClasses(props).join(" ")}`.trim() }>
    { props.children }
  </div>
)

export const Layout = props => (
  <div className={ `Layout ${LayoutClasses(props).join(" ").trim()}`.trim() }>
    { props.children }
  </div>
) // END Layout

export const Wrapper = props => (
  <div className="Wrapper">
    { props.children }
  </div>
) // END Wrapper
