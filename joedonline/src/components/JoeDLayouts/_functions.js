export function ContainerClasses(props) {
  const align = props => props.align === "main-start"
    ? "align-main--start" :
      props.align === "main-end"
      ? "align-main--end" :
        props.align === "cross-start"
        ? "align-cross--start" :
          props.align === "cross-end"
          ? "align-cross--end" : ""

  const centered = props => props.centered === "main"
    ? "centered--main" :
      props.centered === "cross"
      ? "centered--cross" :
        props.centered === "main-cross"
        ? "centered--xy" : ""

  const fullHeight = props => props.fullHeight === "elastic"
    ? "fullheight--elastic" :
      props.fullHeight === "static"
      ? "fullheight--static" : ""

  const gutterBottom = props => props.gutterBottom === "normal"
    ? "gutter-bottom" :
      props.gutterBottom === "narrow"
      ? "gutter-bottom--narrow" :
        props.gutterBottom === "wide"
        ? "gutter-bottom--wide" : ""

  const gutterLeft = props => props.gutterLeft === "normal"
    ? "gutter-left" :
      props.gutterLeft === "narrow"
      ? "gutter-left--narrow" :
        props.gutterLeft === "wide"
        ? "gutter-left--wide" : ""

  const gutterRight = props => props.gutterRight === "normal"
    ? "gutter-right" :
      props.gutterRight === "narrow"
      ? "gutter-right--narrow" :
        props.gutterRight === "wide"
        ? "gutter-right--wide" : ""

  const gutterTop = props => props.gutterTop === "normal"
    ? "gutter-top" :
      props.gutterTop === "narrow"
      ? "gutter-top--narrow" :
        props.gutterTop === "wide"
        ? "gutter-top--wide" : ""

  const padding = props => props.padding === "normal"
    ? "padded" :
      props.padding === "narrow"
      ? "padded--narrow" :
        props.padding === "wide"
        ? "padded--wide" : ""

  return [
    padding(props),
    gutterTop(props),
    gutterBottom(props),
    gutterLeft(props),
    gutterRight(props),
    centered(props),
    align(props),
    fullHeight(props),
  ]
} // END ContainerClasses



export function FlexWrapperClasses(props) {
  const partition = props => props.partition === 10
    ? "width--10" :
      props.partition === 9
      ? "width--9" :
        props.partition === 8
        ? "width--8" :
          props.partition === 7
          ? "width--7" :
            props.partition === 6
            ? "width--6" :
              props.partition === 5
              ? "width--5" :
                props.partition === 4
                ? "width--4" :
                  props.partition === 3
                  ? "width--3" :
                    props.partition === 2
                    ? "width--2" :
                      props.partition === 1
                      ? "width--1" : ""

  return [
    partition(props),
  ]
} // END FlexWrapperClasses



export function LayoutClasses(props) {
  const direction = props => props.direction === "vertical"
    ? "direction--vertical" :
      props.direction === "horizontal"
      ? "direction--horizontal" : ""

  return [
    direction(props),
  ]
} // END LayoutClasses
