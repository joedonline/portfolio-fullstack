# :european_castle: My Portfolio Website - Main :statue_of_liberty:    

### TO DO:
- Main Layout
- Site Documentation Page
- Resume Page

![Spacer: 112 x 56](assets/Spacer_112x56.png)

---
## :art: Prototyping Iterations

![Spacer: 112 x 24](assets/Spacer_112x24.png)

### Iteration 1

![Iteration 1](assets/joedonline__Iteration_1.png)

![Spacer: 112 x 56](assets/Spacer_112x56.png)

### Iteration 2

![Iteration 2](assets/joedonline__Iteration_2.png)

![Spacer: 112 x 56](assets/Spacer_112x56.png)

---
## :pencil: DOCUMENTATION    

![React Logo](assets/reactlogo192.png)    

![Spacer: 112 x 56](assets/Spacer_112x56.png)


### :file_folder: Folder Structure (Displaying up to 4th tier only)

##### ** Root = joedonline/
| Tier 1 | Tier 2 | Tier 3 | Tier 4 |
|-|-|-|-|
| **`public/`** ||||
|| `_redirects` |||
|| `favicon.ico` |||
|| `index.html` |||
|| `manifest.json` |||
|| `robots.txt` |||
| **`src/`** ||||
|| `assets/` |||
||| `scss/` ||
|||| `reset/` |
|||| `typography/` |
|||| `utilities/` |
|||| `index.scss` |
|| `components/` |||
||| `JoeDLayouts/` ||
|||| `scss/` |
|||| `_functions.js` |
|||| `JoeDLayouts.js` |
||| `JoeDMaterials/` ||
|||| `JoeDIcon/` |
||| `MaterialUI/` ||
|||| `AppBars/` |
|||| `Cards/` |
|||| `Drawers/` |
|| `views/` |||
|||| `LandingPage/` |
||| `LandingPage` ||
|| `index.js` |||
|| `serviceWorker.js` |||
| **`.gitignore`** ||||
| **`package.json`** ||||
| **`README.md`** ||||   

![Spacer: 112 x 56](assets/Spacer_112x56.png)

### :bike: Components:
* ## JoeDLayouts.js

    | Members | Description | Total Props | Notes |
    |-|-|-|-|
    | Container | Main area wrapper, to contain groups of elements. Usually an outer-wrapper for *FlexWrapper* but can also be used to wrap single elements. | 8 | _More details below (JoeDLayouts.js Members)_ |
    | FlexWrapper | Divides containers into smaller areas. | 1 | _More details below (JoeDLayouts.js Members)_ |
    | Layout | Sets the "layout" of a container whether it will be a row or a column. | 1 | _More details below (JoeDLayouts.js Members)_ |
    | Wrapper | Use as an outer wrapper. This is just a work-around for center-aligned Containers. | 1 | _More details below (JoeDLayouts.js Members)_ |

    ![Spacer: 112 x 56](assets/Spacer_112x56.png)

* ## JoeDLayouts.js Members

    ##### Container
    | Props | Possible Values | Notes |
    |-|-|-|
    | align | `main-start`, `main-end`, `cross-start`, `cross-end` | _Values are named after CSS Flexbox's **main-axis** & **cross-axis**. ( [Visit this page for more info](https://css-tricks.com/snippets/css/a-guide-to-flexbox/) )_ |
    | centered | `main`, `cross`, `main-cross` | _main-cross aligns both **vertical** and **horizontal** axes to absolute middle._ |
    | fullheight | `elastic`, `static` | _elastic = min-height, static = height (not max-height!)_ |
    | gutterBottom | `normal`, `narrow`, `wide` | _ |
    | gutterLeft | `normal`, `narrow`, `wide` | _ |
    | gutterRight | `normal`, `narrow`, `wide` | _ |
    | gutterTop | `normal`, `narrow`, `wide` | _ |
    | padding | `normal`, `narrow`, `wide` | _Adds padding all around the box._ |    

    ![Spacer: 112 x 56](assets/Spacer_112x56.png)

    ##### FlexWrapper
    | Props | Possible Values | Notes |
    |-|-|-|
    | partition | `1`, `2`, `3`, `4`, `5` | _{ 1 = 1/5, ..., 5 = 5/5 } x 100% of viewport width_ |   

    ![Spacer: 112 x 56](assets/Spacer_112x56.png)

    ##### Layout
    | Props | Possible Values | Notes |
    |-|-|-|
    | direction | `vertical`, `horizontal` | _`vertical` is column. `horizontal` is row. The values are named after CSS Flexbox's main and cross axes. ( [More info here](https://css-tricks.com/snippets/css/a-guide-to-flexbox/) )_ |     

    ![Spacer: 112 x 56](assets/Spacer_112x56.png)   

    ##### Wrapper
    | Props | Possible Values | Notes |
    |-|-|-|
    | n/a | n/a | _Flexbox, centered container. Width is set to 100%_ |
    
    ![Spacer: 112 x 56](assets/Spacer_112x56.png)    

* ## JoeDMaterials/

    | Component | Members | Usage | Notes |
    |-|-|-|-|
    | JoeDCards.js | JoeDCard | `import { JoeDCard as AnyFooBar } from './path/to/JoeDCards.js'` | _See below ( JoeDCards.js Members )_ | 
    | JoeDIcon.js | JoeDIcon | `import { JoeDIcon as AnyFooBar } from './path/to/JoeDIcon.js'` | _See below ( JoeDCards.js Members )_ |
    
    ![Spacer: 112 x 56](assets/Spacer_112x56.png)
    
* ## JoeDCards.js Members
    
    ##### JoeDCard

    | Props | Possible Values | Notes |
    |-|-|-|
    | tbd | tbd | tbd |

    ![Spacer: 112 x 56](assets/Spacer_112x56.png)

* ## JoeDIcon.js Members

    ##### JoeDIcon

    | Props | Possible Values | Notes |
    |-|-|-|
    | fill | `rgb(values)`, `rgba(values)`, `#hex`, `hsl(value)`, `hsla(values)` | _ |
    | isPointer | `true`, `false` | _Adds `cursor: pointer` if set to true._ |
    | name | `Home`, `GitHub`, `Gmail`, `LinkedIn` | _IconFactory looks for these values. Defaults to **GenericIcon** if none given._ |
    | size | Values in { `px`, `em`, `rem`, `vmin`, `vmax` } | _10rem : 16px_ |

    ![Spacer: 112 x 56](assets/Spacer_112x56.png)

    ##### JoeDIcon.js Local Dependencies

    | Component | Local Folder/Path | Members | Notes |
    |-|-|-|-|
    | icons.js | ./JoeDIcon/Icons/ | `Home`, `Generic`, `GitHub`, `Gmail`, `LinkedIn`, `PersonalInfo` | _See below ( icons.js Members )_ |

    ![Spacer: 112 x 56](assets/Spacer_112x56.png)

    ##### icons.js Members
    | Name | Icon Image | Notes |
    |-|:-:|-|
    | Home | ![Home Icon](assets/icon__home.png) | _ |
    | Generic | ![Generic Icon](assets/icon__generic.png) | _Fallback icon if no `name` prop given._ |
    | GitHub | ![GitHub Icon](assets/icon__github.png) | _ |
    | Gmail | ![Gmail Icon](assets/icon__gmail.png) | _ |
    | LinkedIn | ![LinkedIn Icon](assets/icon__linkedin.png) | _ |
    | PersonalInfo | ![LinkedIn Icon](assets/icon__personal_info.png) | _ |

    ![Spacer: 112 x 56](assets/Spacer_112x56.png)

* ## MaterialUI/

    | Component | Category/Folder | Members | Notes |
    |-|-|-|-|
    | SimpleAppBar.js | AppBars/ | n/a | tbd |
    | SimpleCard.js | Cards/ | n/a | tbd |
    | SwipeableDrawer.js | Drawers/ | n/a | tbd |
    | SwipeableDrawerBottom.js | Drawers/ | n/a | tbd |

    ![Spacer: 112 x 56](assets/Spacer_112x56.png)
    
---

# Code Samples

![Spacer: 112 x 24](assets/Spacer_112x24.png)

### JoeDCard

``` jsx

import { Container, FlexWrapper } from "./path/to/JoeDLayouts"
import { JoeDCard as FooBarCard } from "./path/to/JoeDCards"

function MyComponent(props) {
  return (
    <Container fullheight="static" gutterBottom="wide">
      <FlexWrapper partition={ 1 }>
        <FooBarCard />
      </FlexWrapper>
    </Container>
  )
}

export default MyComponent

```

![Spacer: 112 x 56](assets/Spacer_112x56.png)

### JoeDIcon

``` jsx

import { FlexWrapper } from "./path/to/JoeDLayouts"
import { JoeDIcon as GitHubIcon } from "./path/to/JoeDIcon"

function MyComponent(props) {
  const args = {
    fill: "#333",
    isPointer: true,
    name: "GitHub",
    size: "36px",
  }

  return (
    <FlexWrapper partition={ 1 }>
      <GithubIcon { ...args } >
    </FlexWrapper>
  )
}

export default MyComponent
```

![Spacer: 112 x 56](assets/Spacer_112x56.png)
