import $ from 'jquery';

const RESTROOT = 'https://content-api-v1.joedomingo.online';

export const setToken = () => {
  clearReactDevLastSession();

  return $.ajax({
    url: `${RESTROOT}/wp-json/jwt-auth/v1/token`,
    method: `POST`,
    data: {
      'username': process.env.REACT_APP_USER,
      'password': process.env.REACT_APP_PASS,
    }
  })

  .done( response => {
    window.sessionStorage.setItem('newToken', response.token);
  })

  .fail(res => console.error("REST error."));
}

export const updatePost = (content, endpoint, token) => {
  $.ajax({
    url: `${RESTROOT}/wp-json/wp/v2/${endpoint}`,
    method: `POST`,
    beforeSend: xhr => {
      xhr.setRequestHeader( 'Authorization', `Bearer ${token}` );
    },
    data: {
      "title": content.title,
    }
  })
}

export const clearToken = () => window.sessionStorage.removeItem('newToken');
export const clearReactDevLastSession = () => window.sessionStorage.removeItem('React::DevTools::lastSelection');
