export default () => {
  const host = "https://content-api-v1.joedomingo.online";
  const path = "wp-json/jwt-auth/v1/token";

  return fetch(`${host}/${path}`, {
    "method": "POST",
    "body": JSON.stringify({
      "username": `${process.env.REACT_APP_USER}`,
      "password": `${process.env.REACT_APP_PASS}`,
    }),
    "headers": {
      "Content-Type": "application/json",
    },
  })
  .then(response => response.json())
  .catch(err => err.message);
};
