import React, { useState, useEffect } from 'react';
import logo from './logo.svg';
import './App.css';
import Title from './Title';

import dataGET from './data-get';
import { setToken, updatePost } from './data-post';
import sanitize from 'sanitize-html';

const UserData = React.createContext();
export const UserConsumer = UserData.Consumer;

function App() {
  const [data, setData] = useState({});
  const [seshToken, setSeshToken] = useState(null);

  useEffect(() => {
    dataGET("posts").then( data => setData({ title: data[0].title.rendered, content: data[0].acf.test_content }));
  }, []);

  useEffect(() => {
    setToken().done( res => setSeshToken(res.token));
  }, []);

  const changeTitle = event => {
    const contents = {
      title: `Pour-over 3 Wolf Moon Poutine Mustache: ${event.target.value}`,
      content: data.content,
    };

    setToken().done( res => setSeshToken(res.token));
    updatePost(contents, "posts/9", seshToken);
    setData({...contents});
  };

  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <UserData.Provider value={data}>
          <Title />
        </UserData.Provider>
        <button onClick={changeTitle} value="TEXT A">CHANGE TITLE A</button>
        <button onClick={changeTitle} value="TEXT B">CHANGE TITLE B</button>
        <article dangerouslySetInnerHTML={{ __html: sanitize(data.content ? data.content : "LOADING CONTENT...") }} />
      </header>
    </div>
  );
}

export default App;
