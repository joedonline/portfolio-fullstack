import React from 'react';
import { UserConsumer } from './App';

const Title = props => <>
  <UserConsumer>
    {
      ({ title }) => <p>{ !!title ? title : "FETCHING TITLE..." }</p>
    }
  </UserConsumer>
</>;

export default Title;
