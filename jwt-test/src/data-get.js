const host = "https://content-api-v1.joedomingo.online";
const path = "wp-json/wp/v2";

const params = {
  "method": "GET",
  "headers": {},
};

export default (endpoint) =>
  fetch(`${host}/${path}/${endpoint}`, { ...params })
    .then( res => res.json())
    .catch( err => err.message);
